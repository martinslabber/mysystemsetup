#!/bin/bash

# sudo pip install -U yapf docformatter
FILE=$1

echo "Cleanup file $FILE"
docformatter -i $FILE
isort $FILE
yapf --no-local-style -i $FILE
