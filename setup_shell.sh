#!/bin/bash

brew install zsh

curl -L https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh | sh

BASH_RC=~/.bashrc
if [ ! -f $BASH_RC ]
then
	echo "Create ZSH"
	echo 'alias ll="ls -l"' >> $BASH_RC
fi
#zsh help to work...
  #unalias run-help
  #autoload run-help
  #HELPDIR=/usr/local/share/zsh/helpfiles
