#!/bin/bash

echo "Configure Vim"

if [  ! -d ~/.vim/bundle/vundle ]
then
    echo "Install Vundle"
    git clone https://github.com/gmarik/vundle.git ~/.vim/bundle/vundle
else
    echo "Update Vundle"
    cd ~/.vim/bundle/vundle
    git pull
fi

vim +BundleInstall +qall
